package magneto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Magneto's mutant recruiter
 * 
 * @author adrian.prause
 *
 */
public class MutantRecruiter {

	private static int suencuecesToFind = 2;
	private static Set<String> mutantSecuences = new HashSet<String>(Arrays.asList("AAAA", "TTTT", "CCCC", "GGGG"));
	private static int secuenceSize = 4;

	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println("Please give me an DNA. For example: ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG");
		} else {
			if (isMutant(args)) {
				System.out.println("The given DNA is mutant!");
			} else {
				System.out.println("The given DNA is not mutant");
			}
		}

	}

	/**
	 * Check if DNA is mutant
	 * 
	 * @param dna dna table
	 * @return true/false
	 */
	public static boolean isMutant(String[] dna) {

		return MutantRecruiter.isMutant(dna, false);

	}

	/**
	 * Check if DNA is mutant
	 * 
	 * @param dna   dna table
	 * @param debug shows output
	 * @return true/false
	 */
	public static boolean isMutant(String[] dna, boolean debug) {

		long startTime = System.currentTimeMillis();

		int found = 0;
		int length = dna.length;

		if (length < secuenceSize) {
			System.out.println("Not a valid DNA syntax, length should be greater than " + secuenceSize);
			return false;
		}

		int y = 0;
		while (y < length && found < suencuecesToFind) {

			int x = 0;
			int maxIndex = length - secuenceSize;

			if (debug)
				System.out.println("Line: " + y);

			while (x < length && found < suencuecesToFind) {

				try {

					if (x <= maxIndex) {
						String horizontalSecuence = getHorizontalSecuence(x, y, dna);
						if (debug)
							System.out.println("Checking horizontal secuence on " + getCoordsString(x, y) + ": "
									+ horizontalSecuence);
						found += mutantSecuences.contains(horizontalSecuence) ? 1 : 0;
					}

					if (y <= maxIndex && found < suencuecesToFind) {
						String verticalSecuence = getVerticalSecuence(x, y, dna);
						if (debug)
							System.out.println(
									"Checking vertical secuence on " + getCoordsString(x, y) + ": " + verticalSecuence);
						found += mutantSecuences.contains(verticalSecuence) ? 1 : 0;
					}

					if (x <= maxIndex && y <= maxIndex && found < suencuecesToFind) {
						String diagonalSecuence = getDiagonalSecuence(x, y, dna);
						if (debug)
							System.out.println(
									"Checking diagonal secuence on " + getCoordsString(x, y) + ": " + diagonalSecuence);
						found += mutantSecuences.contains(diagonalSecuence) ? 1 : 0;
					}

					if (x >= secuenceSize && y <= maxIndex && found < suencuecesToFind) {
						String inverseDiagonalSecuence = getInverseDiagonalSecuence(x, y, dna);
						if (debug)
							System.out.println("Checking inverse diagonal secuence on " + getCoordsString(x, y) + ": "
									+ inverseDiagonalSecuence);
						found += mutantSecuences.contains(inverseDiagonalSecuence) ? 1 : 0;
					}

				} catch (StringIndexOutOfBoundsException e) {
					System.out.println("Not a valid DNA syntax. Not a n:n matrix.");
					System.out.println("Time ellapsed: " + (System.currentTimeMillis() - startTime) + " milliseconds.");
					return false;

				}

				x++;

			}

			y++;

		}

		if (debug) {
			System.out.println("Mutant secuences found: " + found);
			System.out.println("Time ellapsed: " + (System.currentTimeMillis() - startTime) + " milliseconds.");
		}

		return found >= suencuecesToFind;

	}

	private static String getCoordsString(int x, int y) {
		return (x + 1) + ", " + (y + 1);
	}

	private static String getHorizontalSecuence(int x, int y, String[] dna) {
		return dna[y].substring(x, x + 4);
	}

	private static String getVerticalSecuence(int x, int y, String[] dna) {
		return dna[y].substring(x, x + 1) + dna[y + 1].substring(x, x + 1) + dna[y + 2].substring(x, x + 1)
				+ dna[y + 3].substring(x, x + 1);
	}

	private static String getDiagonalSecuence(int x, int y, String[] dna) {
		return dna[y].substring(x, x + 1) + dna[y + 1].substring(x + 1, x + 2) + dna[y + 2].substring(x + 2, x + 3)
				+ dna[y + 3].substring(x + 3, x + 4);
	}

	private static String getInverseDiagonalSecuence(int x, int y, String[] dna) {
		return dna[y].substring(x, x + 1) + dna[y + 1].substring(x - 1, x) + dna[y + 2].substring(x - 2, x - 1)
				+ dna[y + 3].substring(x - 3, x - 2);
	}

}
