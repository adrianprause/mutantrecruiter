# Reclutador de mutantes de Magneto

Ejercicio práctico de algoritmo de detección de ADN mutante.

### Pre-requisitos 📋

Para el funcionamiento del proyecto, es necesario contar con Maven y Java instalado. Maven te servirá para compilar el codigo fuente y generar el ejecutable.
También es necesario tener permiso sopbre el repositorio.

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

Posicionate sobre una carpeta donde descargar el código fuente y en la consola escribe:

git clone https://gitlab.com/adrianprause/mutantrecruiter.git


### Instalación 🔧

Para compilar el proyecto sigue los siguientes pasos:
Posicionate sobre la carpeta que contiene el código fuente y en la consola escribe:
mvn package

Si todo sale bien, debería haberte ejecutado las pruebas y decirte: BUILD SUCCESS

## Ejecutando las pruebas ⚙️

Para ejecutar las pruebas unitarias del proyecto sigue los siguientes pasos:
Posicionate sobre la carpeta que contiene el código fuente y en la consola escribe:
mvn test

### Uso de la herramienta 🔩

Para ejecutar el detector de ADN mutante sigue los siguientes pasos:

Posicionate sobre la carpeta que contiene el código fuente y en la consola escribe:
java -jar target/mutant-recruiter-1.0-SNAPSHOT.jar

Al no recibir como parámetros secuencias de ADN, el programa te indicara en pantalla como ejecutar un ejemplo:
java -jar target/mutant-recruiter-1.0-SNAPSHOT.jar ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG

Las secuencias simplemente se pasan como parametros separados.

## Autores ✒️


* **Adrián Prause** - *Trabajo Inicial* - (https://gitlab.com/adrianprause)
